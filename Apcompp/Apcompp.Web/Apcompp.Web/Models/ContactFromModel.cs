﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Apcompp.Web.Models
{
    public class ContactFromModel
    {
        [Required]
        [StringLength(100)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [StringLength(100)]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(100)]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [StringLength(100)]
        [Display(Name = "City")]
        public string City { get; set; }

        [StringLength(100)]
        [Display(Name = "State")]
        public string State { get; set; }

        [Display(Name = "Local Manufacturer Rep")]
        public string LocalManufacturerRep { get; set; }

        [Display(Name = "Comments")]
        public string Comments { get; set; }

        [Display(Name = "I don’t know")]
        public bool IDontKnow { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        public DateTime? CreateDt { get; set; }
    }
}