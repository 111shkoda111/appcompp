﻿function goTo(obj) {
    $('html,body').animate({ scrollTop: $(obj).offset().top }, 'slow');
}

function calcPaddingLine(times) {
    if (!times) {
        times = 1
    }
    //16.375% - width shield in %. If logo width 800px shield width 131px.
    var logoPer = $('.logo').width() * 0.01;
    var a = 50 + (16.375 * logoPer) + logoPer * times;
    $('.header-line').css({ 'padding-left': a + 'px' });
}

$(document).ready(function () {

    if ($(window).width() <= 520) {
        calcPaddingLine(-8);
    } else {
        calcPaddingLine();
    }

    $(window).bind('resize', function () {
        if ($(this).width() <= 520) {
            calcPaddingLine(-8);
        } else {
            calcPaddingLine();
        }
    });


    $('.close').click(function () {
        swal.close();
    });

    $('#purchase_power').click(function (e) {
        e.preventDefault();

        var slider = document.getElementById("info-modal");
        slider.style.display = "initial";
        swal({
            title: "",
            content: slider,
            type: "info",
            button: false,
            className: "powered-modal",
            closeOnClickOutside: false,
        });
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() >= 50) {
            $('#back-to-top').fadeIn("slow");
        } else {
            $('#back-to-top').fadeOut("slow");
        }
    });
    $('#back-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
    });

    $('#go-getpowered').click(function () {
        goTo('#powered');
    });

    $("#go-discover").click(function () {
        goTo('#discover');
    });

    $("#go-contact").click(function () {
        if ($('#contact').height() >= $(window).height()) {
            goTo('#contact');
        } else {
            goTo('#bonus');
        }
    });

});