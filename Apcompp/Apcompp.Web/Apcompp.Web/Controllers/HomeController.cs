﻿using Apcompp.Web.Models;
using Apcompp.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Apcompp.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SubmitForm(ContactFromModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Message = "Model is not valid", Code = "Error" });
            }

            if (!model.IDontKnow && String.IsNullOrEmpty(model.LocalManufacturerRep))
            {
                return Json(new { Message = "Model is not valid", Code = "Error" });
            }

            var mail = new EmailService("productpreservers.com");
            try
            {
                mail.SendContactForm(model, ControllerContext);
            }
            catch
            {
                return Json(new { Message = "Something gone wrong", Code = "Error" });
            }

            model.CreateDt = DateTime.Now;

            XmlService.AddToXml<ContactFromModel>("\\App_Data\\ContactForm.xml", model);


            return Json(new { Message = "Succsess!", Code = "Succsess" });

        }
    }
}