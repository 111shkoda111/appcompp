﻿using Apcompp.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;

namespace Apcompp.Web.Services
{
    public class EmailService
    {
        private readonly string _sender;
        private readonly string _recipient;
        private readonly string _recipientSecond;
        private readonly string _recipientThird;
        private string _subjectPrefix;
        public EmailService(string subjectPrefix, string sender = "productpreservers@2-com.net")
        {
            _sender = sender;
            _recipient = System.Configuration.ConfigurationManager.AppSettings["FormContactEmail"];
            _recipientSecond = System.Configuration.ConfigurationManager.AppSettings["FormContactEmailSecond"];
            _recipientThird = System.Configuration.ConfigurationManager.AppSettings["FormContactEmailThird"];
            _subjectPrefix = subjectPrefix;
        }

        public void SendEmail(string subject, string body)
        {
            SendEmail(_recipient, subject, body, null);
            SendEmail(_recipientSecond, subject, body, null);
            SendEmail(_recipientThird, subject, body, null);
        }

        public void SendEmail(string to, string subject, string body, List<string> attachments)
        {
            var client = new SmtpClient("127.0.0.1", 25);
            var message = new MailMessage(_sender, to)
            {
                Subject = string.Format("[{0}] {1}", _subjectPrefix, subject),
                IsBodyHtml = true,
                Body = body
            };

            if (attachments != null)
            {
                foreach (string attachment in attachments)
                {
                    message.Attachments.Add(new Attachment(attachment));
                }
            }

            try
            {
                client.Send(message);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                if (ex.InnerExceptions.Select(t => t.StatusCode).Any(status => status == SmtpStatusCode.MailboxBusy || status == SmtpStatusCode.MailboxUnavailable))
                {
                    System.Threading.Thread.Sleep(750);
                    client.Send(message);
                }
            }
        }

        public void SendContactForm(ContactFromModel model, ControllerContext controllerContext)
        {
            var html = RenderViewToString<ContactFromModel>("~/Views/Shared/ContactForm.cshtml", model, controllerContext);
            var subject = "Contact Us Submission";
            SendEmail(subject, html);
        }

        private string RenderViewToString<T>(string viewPath, T model, ControllerContext controllerContext)
        {
            using (var writer = new StringWriter())
            {
                var view = new RazorView(controllerContext, viewPath, null, false, null);
                var vdd = new ViewDataDictionary<T>(model);
                var viewCxt = new ViewContext(controllerContext, view, vdd, new TempDataDictionary(), writer);
                viewCxt.View.Render(viewCxt, writer);
                return writer.ToString();
            }
        }
    }
}